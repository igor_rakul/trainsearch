﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainSearch.Configuration
{
    public static class Configuration
    {
        private static string baseUrl = "https://bilet.privatbank.ua/";

        private static string sessionInitEndpoint = "sm/train/init";

        private static string trainSearchEndpoint = "sm/train/search.json";

        private static string seatSearchEndpoint = "sm/train/train.json";

        public static string InitSessionEndpoint => baseUrl + sessionInitEndpoint;

        public static string TrainSearchEndpoint => baseUrl + trainSearchEndpoint;

        public static string SeatSearchEndpoint => baseUrl + seatSearchEndpoint;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TrainSearch.Models;
using Newtonsoft.Json;
using TrainSearch.Services;

namespace TrainSearch.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(ITrainSearchClient trainSearchClient)
        {
            TrainSearchClient = trainSearchClient;
        }

        public ITrainSearchClient TrainSearchClient { get; set; }

        public async Task<IActionResult> Index()
        {
            var condition = new TrainFilterCondition
            {
                CustomerDepartureStationId = StationCode.Kyiv,
                CustomerArrivalStationId = StationCode.KryvyiRih,
                CustomerDepartureDate = new DateTime(2020, 6, 25),
                PreferredCarCode = CarCode.SecondClassSeat,
                SeatType = SeatType.Lower
            };

            var model = await TrainSearchClient.GetAvailableSeats(condition);

            return Ok(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
                
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

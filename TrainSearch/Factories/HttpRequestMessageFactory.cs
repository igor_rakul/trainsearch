﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using TrainSearch.Models;
using Newtonsoft.Json;
using System.Net;

using Encoding = System.Text.Encoding;

namespace TrainSearch.Factories
{
    public static class HttpRequestMessageFactory
    {
        public static HttpRequestMessage BuildHttpPostRequestMessage(Request request , string requestUri)
        {
            
            var serializedRequest = JsonConvert.SerializeObject(request);
            var content = new StringContent(serializedRequest, Encoding.UTF8, "application/json");

            var httpPostRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(requestUri),
                Content = content,
                Version = HttpVersion.Version11
            };
            
            httpPostRequestMessage.Headers.Add("Host", httpPostRequestMessage.RequestUri.Host);
            httpPostRequestMessage.Content.Headers.ContentLength = serializedRequest.Length;
             
            return httpPostRequestMessage;
        }

        public static HttpRequestMessage BuildHttpGetRequestMessage(Request request, string requestUri)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainSearch.Models;

namespace TrainSearch.Factories
{
    public static class SessionRequestFactory
    {
        public static InitializeSessionRequest BuildInitializeSessionRequest()
        {

            return new InitializeSessionRequest();
        }

        public static InitializeSessionRequest BuildDefaultInitializeSessionRequest()
        {
            return new InitializeSessionRequest
            {
                Identifier = "PbUa",
                Parameters = new Parameters(lang: "uk", locale: "UA"),
                Cs = "6e6d7304-22f2-4798-a5d7-e0dbe1362439",
                UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36",
                Ab = "A"
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainSearch.Models;

namespace TrainSearch.Factories
{
	public class TrainCarRequestFactory
	{
		public static GetAllTrainsRequest BuildGetAllTrainsRequest(string sessionId, TrainFilterCondition condition)
		{
			return new GetAllTrainsRequest
			{
				From = (int)condition.CustomerDepartureStationId,
				To = (int)condition.CustomerArrivalStationId,
				DepartureDate = condition.CustomerDepartureDate,
				SessionId = sessionId,
				Lang = Language.UA.ToString("g")
			};
		}

		public static GetAllTrainsRequest BuildDefaultGetAllTrainsRequest(string sessionId)
		{
			return new GetAllTrainsRequest
			{
				From = (int)StationCode.Kyiv,
				To = (int)StationCode.KryvyiRih,
				DepartureDate = new DateTime(2020, 5, 30),
				SessionId = sessionId,
				Lang = Language.UA.ToString("g")
			};
		}

		public static GetAllCarsRequest BuildGetAllCarsRequest(string sessionId, string trainHash)
		{
			return new GetAllCarsRequest
			{
				TrainHash = trainHash,
				SessionId = sessionId,
				Lang = Language.UA.ToString("g"),
				TrainInfo = true
			};
		}
	}
}

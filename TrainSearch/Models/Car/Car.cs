﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TrainSearch.Models
{
	public class Car
	{
		[JsonProperty("hash")]
		public string Hash { get; set; }

		[JsonProperty("number")]
		public int Number { get; set; }

		[JsonProperty("price")]
		public decimal Price { get; set; }

		[JsonProperty("bookingPrice")]
		public string BookingPrice { get; set; }

		[JsonProperty("typeId")]
		public int TypeId { get; set; }

		[JsonProperty("freeSeats")]
		public int FreeSeats { get; set; }

		[JsonProperty("lowerSeats")]
		public int LowerSeats { get; set; }

		[JsonProperty("upperSeats")]
		public int UpperSeats { get; set; }

		[JsonProperty("sideLowerSeats")]	
		public int SideLowerSeats { get; set; }

		[JsonProperty("sideUpperSeats")]
		public int SideUpperSeats { get; set; }

		[JsonProperty("isTransformer")]
		public bool IsTransformer { get; set; }
	}
}

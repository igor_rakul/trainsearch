﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainSearch.Models
{
    public enum CarCode
    {
        Coupe = 2,

        Platzkart = 3,

        FirstClassSeat = 13,

        SecondClassSeat = 14
    }
}

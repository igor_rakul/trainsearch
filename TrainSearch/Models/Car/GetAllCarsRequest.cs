﻿using Newtonsoft.Json;

namespace TrainSearch.Models
{
    public class GetAllCarsRequest : Request
    {
        [JsonProperty("trainHash")]
        public string TrainHash { get; set; }

        [JsonProperty("sessionId")]
        public string SessionId { get; set; }

        [JsonProperty("lang")]
        public string Lang { get; set; }

        [JsonProperty("trainInfo")]
        public bool TrainInfo { get; set; }
    }
}
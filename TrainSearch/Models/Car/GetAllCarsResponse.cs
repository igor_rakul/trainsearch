﻿using System.Collections.Generic;

namespace TrainSearch.Models
{
	public class GetAllCarsResponse : Response
	{
		public IEnumerable<Car> Cars { get; set; }

		public IEnumerable<CarType> CarTypes { get; set; }

		public Train Train { get; set; }

	}
}
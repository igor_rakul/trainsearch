﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainSearch.Models
{
	public enum SeatType
	{
		Lower = 1,

		Upper = 2,

		SideLower = 3,

		SideUpper = 4
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainSearch.Models
{
	public enum Language
	{
		UA = 0,

		RU = 1, 

		EN = 2
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TrainSearch.Models
{
    public class InitializeSessionRequest : Request
    {
        [JsonProperty("identifier")]
        public string Identifier { get; set; }

        [JsonProperty("parameters")]
        public Parameters Parameters { get; set; }

        [JsonProperty("cs")]
        public string Cs { get; set; }

        [JsonProperty("userAgent")]
        public string UserAgent { get; set; }

        [JsonProperty("ab")]
        public string Ab { get; set; }
    }
}

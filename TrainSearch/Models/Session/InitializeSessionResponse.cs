﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TrainSearch.Models
{
    public class InitializeSessionResponse : Response
    {
        [JsonProperty("withBedclothes")]
        public bool WithBedclothes { get; set; }

        [JsonProperty("guaranteeMinPrice")]
        public int GuaranteeMinPrice { get; set; }

        [JsonProperty("cashBackMethod")]
        public CashBackMethod CashBackMethod { get; set; }

        [JsonProperty("seatsLimit")]
        public int SeatsLimit { get; set; }

        [JsonProperty("ivrCheckInterval")]
        public int IvrCheckInterval { get; set; }

        [JsonProperty("disableOnLoad")]
        public bool DisableOnLoad { get; set; }

        [JsonProperty("guaranteePercent")]
        public int GuaranteePercent { get; set; }

        [JsonProperty("promo")]
        public bool Promo { get; set; }

        [JsonProperty("backTrainMinInterval")]
        public int BackTrainMinInterval { get; set; }

        [JsonProperty("captcha")]
        public bool Captcha { get; set; }

        [JsonProperty("ivrShowDelay")]
        public int IvrShowDelay { get; set; }

        [JsonProperty("bothWaySeatsLimit")]
        public int BothWaySeatsLimit { get; set; }

        [JsonProperty("guaranteeDefaultOn")]
        public bool GuaranteeDefaultOn { get; set; }

        [JsonProperty("trainPriceMode")]
        public string TrainPriceMode { get; set; }

        [JsonProperty("timeDom")]
        public bool TimeDom { get; set; }

        [JsonProperty("waitingListEndDays")]
        public int WaitingListEndDays { get; set; }

        [JsonProperty("forwardPassengers")]
        public bool ForwardPassengers { get; set; }

        [JsonProperty("ivrConfirmTime")]
        public int IvrConfirmTime { get; set; }

        [JsonProperty("partPayMinOrderSum")]
        public int PartPayMinOrderSum { get; set; }

        [JsonProperty("autoRepayment")]
        public bool AutoRepayment { get; set; }

        [JsonProperty("p24TrainArchiveLink")]
        public string P24TrainArchiveLink { get; set; }

        [JsonProperty("sessionId")]
        public string SessionId { get; set; }

        [JsonProperty("broker")]
        public string Broker { get; set; }

        [JsonProperty("searchEndDays")]
        public int SearchEndDays { get; set; }

        [JsonProperty("cs")]
        public string Cs { get; set; }
    }

    public class CashBackMethod
    {

    }
}

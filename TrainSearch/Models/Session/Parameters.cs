﻿using Newtonsoft.Json;

namespace TrainSearch.Models
{
    public class Parameters
    {
        public Parameters(string lang, string locale)
        {
            Lang = lang;
            Locale = locale;
        }

        [JsonProperty("lang")]
        public string Lang { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TrainSearch.Models
{
    public class GetAllTrainsResponse : Response
    {
        [JsonProperty("trains")]
        public IEnumerable<Train> Trains { get; set; }

        [JsonProperty("stations")]
        public IEnumerable<Station> Stations { get; set; }
    }
}
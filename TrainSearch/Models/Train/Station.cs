using System;
using Newtonsoft.Json;

namespace TrainSearch.Models
{
    public class Station
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

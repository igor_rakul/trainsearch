﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TrainSearch.Models
{
    public class Train
    {
        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("passengerDepartureDate")]
        public DateTime PassengerDepartureDate { get; set; }

        [JsonProperty("passengerArrivalDate")]
        public DateTime PassengerArrivalDate { get; set; }

        [JsonProperty("departureStationId")]
        public int DepartureStationId { get; set; }

        [JsonProperty("arrivalStationId")]
        public int ArrivalStationId { get; set; }

        [JsonProperty("passengerDepartureStationId")]
        public int PassengerDepartureStationId { get; set; }

        [JsonProperty("passengerArrivalStationId")]
        public int PassengerArrivalStationId { get; set; }

        [JsonProperty("isElectronic")]
        public bool IsElectronic { get; set; }

        [JsonProperty("isRouteExists")]
        public bool IsRouteExists { get; set; }

        [JsonProperty("carTypes")]
        public IEnumerable<CarType> CarTypes { get; set; }
    }
}

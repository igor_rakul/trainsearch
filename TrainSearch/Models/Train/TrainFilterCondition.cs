﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainSearch.Models
{
    public class TrainFilterCondition
    {
        public StationCode CustomerDepartureStationId { get; set; }

        public StationCode CustomerArrivalStationId { get; set; }

        public DateTime CustomerDepartureDate { get; set; }

        public CarCode PreferredCarCode { get; set; }

        public SeatType SeatType { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TrainSearch.Models;

namespace TrainSearch.Services
{
    public class HttpService : IHttpService
    {
        
        public async Task<HttpResponseMessage> SendRequest(HttpRequestMessage httpRequestMessage)
        {
            using (var httpClient = new HttpClient(new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (message, certificate2, chain, sslPolicyErrors) => true
                }))
            {
                var response = await httpClient.SendAsync(httpRequestMessage);

                
                return response;
            }
        }
    }
}

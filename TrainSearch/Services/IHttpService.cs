﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TrainSearch.Models;

namespace TrainSearch.Services
{
    public interface IHttpService
    {
        Task<HttpResponseMessage> SendRequest(HttpRequestMessage httpRequestMessage);
        
    }
}

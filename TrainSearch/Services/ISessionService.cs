﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainSearch.Models;

namespace TrainSearch.Services
{
    public interface ISessionService
    {
        Task<string> InitializeSessionId();
    }
}

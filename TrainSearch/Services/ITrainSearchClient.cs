﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainSearch.Models;

namespace TrainSearch.Services
{
	public interface ITrainSearchClient
	{
		Task<string> GetAvailableSeats(TrainFilterCondition condition);
	}
}

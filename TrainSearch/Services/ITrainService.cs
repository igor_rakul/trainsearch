﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainSearch.Models;

namespace TrainSearch.Services
{
    public interface ITrainService
    {
        Task<string> GetTrain(string sessionId, TrainFilterCondition condition);

        Task<IEnumerable<string>> GetSeatsInTrain(string sessionId, string trainHash, TrainFilterCondition condition);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainSearch.Models;
using TrainSearch.Factories;
using Newtonsoft.Json;
using EndpointConfig = TrainSearch.Configuration.Configuration;
using System.Net;

namespace TrainSearch.Services
{
	public class SessionService : ISessionService
	{

		public SessionService(IHttpService httpService)
		{
			HttpService = httpService;
		}

		public IHttpService HttpService { get; set; }

		public async Task<string> InitializeSessionId()
		{
			var initializeSessionRequest = SessionRequestFactory.BuildDefaultInitializeSessionRequest();

			var initSessionPostRequestMessage = HttpRequestMessageFactory.BuildHttpPostRequestMessage(initializeSessionRequest, EndpointConfig.InitSessionEndpoint);

			var initSessionResponseMessage = await HttpService.SendRequest(initSessionPostRequestMessage);

			var serializedResponse = await initSessionResponseMessage.Content.ReadAsStringAsync();

			if (initSessionResponseMessage.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception($"{initSessionResponseMessage.StatusCode.ToString()} {serializedResponse}");
			}

			var initSessionResponse = JsonConvert.DeserializeObject<InitializeSessionResponse>(serializedResponse);

			return initSessionResponse.SessionId;
		}
	}
}

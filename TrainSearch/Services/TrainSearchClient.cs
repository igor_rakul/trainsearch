﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TrainSearch.Factories;
using TrainSearch.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TrainSearch.Models;


namespace TrainSearch.Services
{
	public class TrainSearchClient : ITrainSearchClient
	{
		public TrainSearchClient(ITrainService trainService, ISessionService sessionService)
		{
			TrainService = trainService;
			SessionService = sessionService;
		}

		public ITrainService TrainService { get; private set; }
		public ISessionService SessionService { get; private set; }

		public async Task<string> GetAvailableSeats(TrainFilterCondition condition)
		{
			return await GetTrains(condition);
		}

		private async Task<string> GetTrains(TrainFilterCondition condition)
		{
			var sessionId = await SessionService.InitializeSessionId();

			var trainHash = await TrainService.GetTrain(sessionId, condition);

			var selectedCars = await TrainService.GetSeatsInTrain(sessionId, trainHash, condition);

			string trainInfo = string.Empty;

			foreach(var c in selectedCars)
			{
				trainInfo += c + " ";
			}

			return trainInfo;

			//return trainHash.ToString();
		}
	}
}
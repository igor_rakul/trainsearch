﻿using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;
using TrainSearch.Factories;
using TrainSearch.Models;
using EndpointConfig = TrainSearch.Configuration.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;

namespace TrainSearch.Services
{
	public class TrainService : ITrainService

	{
		public TrainService(IHttpService httpService)
		{
			HttpService = httpService;
		}

		public IHttpService HttpService { get; set; }

		public async Task<GetAllCarsResponse> GetAllCars(GetAllCarsRequest getAllCarsRequest)
		{
			var getAllCarsPostRequestMessage = HttpRequestMessageFactory.BuildHttpPostRequestMessage(getAllCarsRequest, EndpointConfig.InitSessionEndpoint);

			var getAllCarsResponseMessage = await HttpService.SendRequest(getAllCarsPostRequestMessage);

			var serializedResponse = await getAllCarsResponseMessage.Content.ReadAsStringAsync();

			var getAllCarsResponse = JsonConvert.DeserializeObject<GetAllCarsResponse>(serializedResponse);

			return getAllCarsResponse;
		}

		public async Task<string> GetTrain(string sessionId, TrainFilterCondition condition)
		{
			var getAllTrainsRequest = TrainCarRequestFactory.BuildGetAllTrainsRequest(sessionId, condition);

			var getAllTrainsPostRequestMessage = HttpRequestMessageFactory.BuildHttpPostRequestMessage(getAllTrainsRequest, EndpointConfig.TrainSearchEndpoint);

			var getAllTrainsResponseMessage = await HttpService.SendRequest(getAllTrainsPostRequestMessage);

			var getAllTrainsResponse = await HandleResponseMessage<GetAllTrainsResponse>(getAllTrainsResponseMessage);

			var trains = getAllTrainsResponse.Trains;

			var filteredTrainHash = trains

				.Where(t => t.DepartureStationId == (int)condition.CustomerDepartureStationId
							&& t.ArrivalStationId == (int)condition.CustomerArrivalStationId)
				.Where(t => t.CarTypes.SingleOrDefault(ct => ct.Id == (int)condition.PreferredCarCode
															&& ct.FreeSeats > 10) != null)
				.Select(t => t.Hash
				//new
				//{
				//	TrainNumber = t.Number,
				//	TrainHash = t.Hash,
				//	NumberOfFreeSeats = t.CarTypes.Where(ct => ct.Id == (int)condition.PreferredCarCode).SingleOrDefault().FreeSeats
				//}.ToString()
				)
				.FirstOrDefault();

			return filteredTrainHash;
		}

		public async Task<IEnumerable<string>> GetSeatsInTrain(string sessionId, string trainHash, TrainFilterCondition condition)
		{
			var getAllCarsRequest = TrainCarRequestFactory.BuildGetAllCarsRequest(sessionId, trainHash);

			var getAllCarsPostRequestMessage = HttpRequestMessageFactory.BuildHttpPostRequestMessage(
				getAllCarsRequest, 
				EndpointConfig.SeatSearchEndpoint);

			var getAllCarsResponseMessage = await HttpService.SendRequest(getAllCarsPostRequestMessage);

			var getAllCarsResponse = await HandleResponseMessage<GetAllCarsResponse>(getAllCarsResponseMessage);

			var cars = getAllCarsResponse.Cars;

			var carTypes = getAllCarsResponse.CarTypes.Where(ct => ct.Id == (int)condition.PreferredCarCode);

			if(carTypes == null)
			{
				return null;
			}

			var filteredCars = cars.Where(c => c.TypeId == (int)condition.PreferredCarCode);

			var selectedCars = filteredCars;

			switch(condition.SeatType)
			{
				case SeatType.Lower:
					selectedCars = filteredCars.Where(c => c.LowerSeats > 5);
					break;
				case SeatType.Upper:
					selectedCars = filteredCars.Where(c => c.UpperSeats > 5);
					break;
				case SeatType.SideLower:
					selectedCars = filteredCars.Where(c => c.SideLowerSeats > 5);
					break;
				case SeatType.SideUpper:
					selectedCars = filteredCars.Where(c => c.SideUpperSeats > 5);
					break;
			}

			var trainInfo = $"Train Number: {getAllCarsResponse.Train.Number}, Car Numbers: ";
			
			return selectedCars.Select(c => c.Number.ToString()).Prepend(trainInfo);
		}

		private async Task<T> HandleResponseMessage<T>(HttpResponseMessage message)
		{ 
			var serializedResponse = await message.Content.ReadAsStringAsync();

			if(message.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception($"{serializedResponse}");
			}

			var response = JsonConvert.DeserializeObject<T>(serializedResponse);

			return response;
		}
	}
}
